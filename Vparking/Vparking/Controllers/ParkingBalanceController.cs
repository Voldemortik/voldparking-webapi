﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vparking.Services;
namespace Vparking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingBalanceController
    {
        private ParkingServices parkingService;
        public ParkingBalanceController()
        {
            parkingService = new ParkingServices();
        }
        [HttpGet]
         public  float  Get()
        {
            return Parking.Balance;
        }

        [HttpGet("minute")]
        public string GetMinuteBalance()
        {

            return parkingService.PaymentsPerMinute().ToString();
        }

    }
}

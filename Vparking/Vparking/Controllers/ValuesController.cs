﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Vparking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

       

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {

        }

       

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            return "deleted " + id;
        }
    }
}

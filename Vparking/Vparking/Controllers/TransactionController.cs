﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Vparking.Services;
namespace Vparking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController
    {
        TransactionServices tss = new TransactionServices();
        [HttpGet]
        public string Get()
        {


            return JsonConvert.SerializeObject(tss.TransactionStringFromFile());
        }
        [HttpGet("minute")]
        public string GetPerMinute()
        {


            return tss.TransactionPerMinute().ToString();
        }

    }
}

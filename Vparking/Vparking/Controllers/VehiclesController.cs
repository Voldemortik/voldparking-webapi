﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vparking.Services;

namespace Vparking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController
    {
        private VehiclesServices vehicleService;
        public VehiclesController()
        {
            vehicleService = new VehiclesServices();
        }

        [HttpGet]
        public string Get()
        {
            return vehicleService.ShowBusyFreeParkingPlaces().ToString();

        }
        [HttpGet("all")]
        public string GetAllVehicles()
        {
            return vehicleService.ShowAllVehicles().ToString();
        }
        
        [HttpGet("addBalance")]
        public string PostBalanceVehicle(int id, double refill)
        {
            return vehicleService.AddBalanceVehicle(id, refill).ToString();
            
        }

        [HttpGet("deletevehicle")]
        public string DeleteVehicle(int id)
        {
            return vehicleService.DeleteVehiclesFromParking(id).ToString();
        }
        [HttpPost]
        public string PostVehicle(Vehicles vehicles)
        {
            return vehicleService.AddVehiclesToParking(vehicles).ToString();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;


using System.Text;

using System.Threading;
using Newtonsoft.Json;
namespace Vparking.Services
{
    public class VehiclesServices
    {
        static public string path = @"..\..\Transaction.log";
        Parking vParking = Startup.voldemortParking.Vpark;
        public TimerCallback tm = new TimerCallback(TimerTransaction);

        public StringBuilder ShowBusyFreeParkingPlaces()
        {
            int deltaPlaces = 10 - vParking.VehicleList.Count;
            StringBuilder result = new StringBuilder($"Кол-во свободных мест : {deltaPlaces}\n" + $"Кол-во занятых мест  : {vParking.VehicleList.Count}\n");

            return result;
        }
       static  public void TimerTransaction(object vehicle)
        {

            Vehicles car = (Vehicles)vehicle;
            Transaction carTransaction = new Transaction(car);
            string jsonConvert = JsonConvert.SerializeObject(carTransaction);

            File.WriteAllText(@path, jsonConvert);
            

        }
        public StringBuilder AddVehiclesToParking(Vehicles vehicle)
        {

            StringBuilder result;
            ParkingPlace parkingID = new ParkingPlace(vParking);
            if (parkingID.vehiclesPlace != -1)
            {
                vehicle.Id = parkingID.vehiclesPlace;
                vehicle.ParkingTime = DateTime.Now;
                vParking.VehicleList.Add(parkingID.vehiclesPlace, vehicle);
                vParking.TimersList.Add(vehicle.Id, new Timer(tm, vehicle, 0, 5000));

                result = new StringBuilder("Спасибо что выбираете именно нас!" + $"Ваш id ={vehicle.Id}");


            }
            else
            {
                result = new StringBuilder("Извините, но сейчас нет свободных мест(");

            }

            return result;
        }
        public StringBuilder DeleteVehiclesFromParking(int vehiclesId)
        {
            StringBuilder result;
            if (vParking.VehicleList.ContainsKey(vehiclesId))
            {

                if (vParking.VehicleList[vehiclesId].Balance >= 0)
                {
                    result = new StringBuilder($"\nВозвращаем ваш остаток средств :{vParking.VehicleList[vehiclesId].Balance}");
                }
                else
                {
                    result = new StringBuilder($"\nВаша задолженность составляет :{Math.Abs(vParking.VehicleList[vehiclesId].Balance)} (Оплатите при выезде)\n");
                }
                vParking.VehicleList.Remove(vehiclesId);

                vParking.TimersList[vehiclesId].Dispose();
                vParking.TimersList.Remove(vehiclesId);
                result.Append($"\nВаше Транспортное средство с id({ vehiclesId}) удалено со списка.С нетерпением ждем вас снова!");


            }
            else
            {

                result = new StringBuilder($"Простите, но Транспортное средство с id ({vehiclesId}) НЕ зарегистрировано. Проверьте  правильность ввода!");

            }
            return result;
        }
        public StringBuilder ShowAllVehicles()
        {
            StringBuilder result = new StringBuilder();
            if (vParking.VehicleList.Count == 0)
            {
                result.Append("Пуст");

            }
            else
            {
                foreach (KeyValuePair<int, Vehicles> entry in vParking.VehicleList)
                {
                    result.Append(entry.Key + ". " + vParking.VehicleList[entry.Key].Name);
                    result.Append("     Дата заезда: " + vParking.VehicleList[entry.Key].ParkingTime);
                    result.Append("     Баланс автомобиля: " + vParking.VehicleList[entry.Key].Balance + "\n");

                }
            }


            return result;

        }
        public StringBuilder AddBalanceVehicle(int vehicleId, double refill)
        {
            StringBuilder result = new StringBuilder();
            if (vParking.VehicleList.ContainsKey(vehicleId))
            {
                vParking.VehicleList[vehicleId].Balance += refill;
                result.Append("Пополнено!");
            }
            else
            {
                result.Append($"Простите, но авто с id ({vehicleId}) НЕ зарегистрировано.Проверьте  правильность ввода!");


            }
            return result;

        }
    }
}

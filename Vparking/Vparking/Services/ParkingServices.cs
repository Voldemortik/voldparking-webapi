﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using System.IO;


using System.Text;

using System.Threading;
using Newtonsoft.Json;

namespace Vparking.Services
{
    public class ParkingServices
    {
        public string path = @"..\..\Transaction.log";
        Parking vParking = Startup.voldemortParking.Vpark;
        
        public StringBuilder PaymentsPerMinute()
        {
            StringBuilder result = new StringBuilder();
            double paymentsSum = 0;
            DateTime minusMinuteTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute - 1, DateTime.Now.Second);

            foreach (Transaction transaction in TransactionFromFile())
            {
                if (transaction.TransactionTime >= minusMinuteTime)
                {
                    paymentsSum += transaction.payment;
                }
            }
           result.Append($"Заработано за минуту: {paymentsSum}");
            return result;
        }
        
        public List<Transaction> TransactionFromFile()
        {
            List<Transaction> transactionList = new List<Transaction>();


            using (StreamReader sr = new StreamReader(@path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    transactionList.Add(JsonConvert.DeserializeObject<Transaction>(line));
                }


            }



            return transactionList;
        }
       


    }
}

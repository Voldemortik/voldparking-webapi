﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.IO;


using System.Text;

using System.Threading;
using Newtonsoft.Json;
namespace Vparking.Services
{
    public class TransactionServices
    {
        public string path = @"..\..\Transaction.log";
        public List<string> TransactionStringFromFile()
        {
            List<string> transactionList = new List<string>();
            

                using (StreamReader sr = new StreamReader(@path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        transactionList.Add(line);
                    }


                }
            
            

            return transactionList;
        }
        public List<Transaction> TransactionFromFile()
        {
            List<Transaction> transactionList = new List<Transaction>();


            using (StreamReader sr = new StreamReader(@path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    transactionList.Add(JsonConvert.DeserializeObject<Transaction>(line));
                }


            }
            
            return transactionList;
        }
        public StringBuilder TransactionPerMinute()
        {
            StringBuilder result = new StringBuilder();
            int count = 1;
      

            foreach (Transaction transaction in TransactionFromFile())
            {
                if (transaction.TransactionTime >= DateTime.Now.AddMinutes(-1))
                {
                    result.Append($"{count}) {transaction.TransactionTime} ({transaction.VehicleIdentificator.Name}),уплачено: {transaction.payment}");
                    count++;
                }
                
            }
            if (result.Length == 0)
            {
               result.Append("К сожалению транзакций не было!\n");
            }
            return result;
        }
    }
}

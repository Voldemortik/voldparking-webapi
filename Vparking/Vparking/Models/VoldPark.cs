﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vparking
{
    public class VoldPark
    {
        public Parking Vpark { get; set; }
        public void Launch(float balance)
        {
           Vpark = Parking.getInstance(balance);
            Vpark.VehicleList = new Dictionary<int, Vehicles>();
            Vpark.TimersList = new Dictionary<int, System.Threading.Timer>();
        }

    }
}

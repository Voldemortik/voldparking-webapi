﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vparking
{
    class ParkingPlace
    {
        public int vehiclesPlace = -1;
        public ParkingPlace(Parking parking)
        {
            if (parking.VehicleList.Keys.Count == 0 )
            {
                vehiclesPlace = 1;
            }
            else
            {
                if (parking.VehicleList.Keys.Count < 10)
                {
                    vehiclesPlace = parking.VehicleList.Keys.Max() + 1;
                }
            }
        }
    }
}

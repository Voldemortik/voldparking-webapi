﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using System.Web;

namespace Vparking
{
   public class Parking
    {
        
       
       
        private static Parking instance;
        public static float Balance { get; set; }
        public  Dictionary<int, Vehicles> VehicleList { get; set; }
        public Dictionary<int, Timer> TimersList { get; set; }
        private List<Transaction> TransacInMinute { get; set; }
        private static object syncRoot = new object();
        

        protected Parking(float balance)
        {
            Balance = balance;
        }

        public static Parking getInstance(float balance)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new Parking(balance);
                }
            }
            return instance;
        }

        
       
        
      
      

        
        
        
       
        
    
    

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vparking
{
    [Serializable]
    public class Transaction
    {
        public DateTime TransactionTime { get; set; }
        public Vehicles VehicleIdentificator { get; set; }
        public float payment { get; set; }
        public Transaction(Vehicles vehicle)
        {
            if(vehicle is Bus)
            {
                VehicleIdentificator = vehicle as Bus;
                payment = InitialSettings.busTariff;
                
                if((VehicleIdentificator.Balance - payment) <= 0)
                {
                    payment *= InitialSettings.penaltyRatio;
                    VehicleIdentificator.Balance -= payment;
                    Parking.Balance += payment ;
                }
                
                else
                {
                    VehicleIdentificator.Balance -= payment;
                    Parking.Balance += payment;
                }
                
                TransactionTime = DateTime.Now;
            }
            if(vehicle is Motorcycle)
            {
                VehicleIdentificator = vehicle as Motorcycle;
                payment = InitialSettings.motorcycleTariff;
                if ((VehicleIdentificator.Balance - payment) <= 0)
                {
                    payment *= InitialSettings.penaltyRatio;
                    VehicleIdentificator.Balance -= payment ;
                    Parking.Balance += payment;
                }
                else
                {
                    VehicleIdentificator.Balance -= payment;
                    Parking.Balance += payment;
                }
                TransactionTime = DateTime.Now;
            }
            if(vehicle is PassengerСar)
            {
                VehicleIdentificator = vehicle as PassengerСar;
                payment = InitialSettings.passengerCarTariff;
                if ((VehicleIdentificator.Balance - payment) <= 0)
                {
                    payment *= InitialSettings.penaltyRatio;
                    VehicleIdentificator.Balance -= payment ;
                    Parking.Balance += payment;
                }
                else
                {
                    VehicleIdentificator.Balance -= payment;
                    Parking.Balance += payment;
                }
                TransactionTime = DateTime.Now;
            }
            if(vehicle is Truck)
            {
                VehicleIdentificator = vehicle as Truck;
                payment = InitialSettings.truckTariff;
                if ((VehicleIdentificator.Balance - payment) <= 0)
                {
                    payment *= InitialSettings.penaltyRatio;
                    VehicleIdentificator.Balance -= payment ;
                    Parking.Balance += payment;
                }
                else
                {
                    VehicleIdentificator.Balance -= payment;
                    Parking.Balance += payment;
                }
                TransactionTime = DateTime.Now;
            }
            
        }

     }
}
